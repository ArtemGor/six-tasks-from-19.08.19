'====================================================================================
'4.Описать класс, представляющий треугольник.
'  Предусмотреть
'   +методы для создания объектов,
'   +вычисления площади и периметра
'   +точки пересечения медиан.
'   +Описать свойства для получения состояния объекта.
'Хочу здесь вводить сам в интерфейсе программы стороны треугольника и видеть по итогу все вышеописанные параметры
'====================================================================================
'=======================================================================================
'=======================================================================================
Class Triangle
	'======================================
	'1.Свойства и переменные
	'======================================
    Private check
    Private result(2)
    Private stringWithEnteredSides
    Public List
    '======================================
    '2.Конструктор/деструктор
    '======================================
    Private Sub Class_Initialize()
    End Sub
   
    Sub Class_Terminate()
    End Sub
    '======================================
    '3.Getters+Setters
    '======================================
	'set lenght of each side
	Public Property Let setSideLenght(sideNumber)
        check = false
        Do
            lenghtOfSide = inputBox( stringWithEnteredSides & vbCr & "Please, enter lenght of side #" & sideNumber + 1 & ":", "Let calc : )")
            if lcase(trim(lenghtofside)) <> "exit" Then
				If isNumeric(lenghtOfSide) = True Then
					If lenghtOfSide > 0 Then
						check = true
					Else
						MsgBox("Lenght must be >>positive<< number in numeric format")
					End If
				Else
					MsgBox("Lenght must be positive number in >>numeric format<<")
				End If
			End if
            If check = true and lcase(trim(lenghtofside)) <> "exit" Then
                stringWithEnteredSides = stringWithEnteredSides & "side #" & sideNumber + 1 & " = " & lenghtOfSide & "; "
                result(sideNumber) = lenghtOfSide
			Else
				result(sideNumber) = lenghtOfSide
            End If
        Loop Until check = true or lcase(trim(lenghtofside)) = "exit"
    End Property
   
   'get lenght of side
    Public Property Get getSideLenght(sideNumber)
    	If lcase(trim(result(sideNumber))) <> "exit" Then
    		getSideLenght = CSng(result(sideNumber))
    	Else
    		getSideLenght = result(sideNumber)
    	End If
    End Property
    
	'Property to calc Perimetr
    Public Property Get perimetr
        perimetr = calcPerimetr(a, b, c)
    End Property
	
	'Property to calc Square
    Public Property Get square
        square = calcSquare(a, b, c)
    End Property
   
    'Point's of Median Intersection (PMI) coordinates xPMI & yPMI:
    Public Property Get xPMI         'Ox = AC, Oy starts in A-point and perpendicular Ox
        xPMI = (a^2 - b^2 + 3*c^2)/6/c
    End Property
   
    Public Property Get yPMI          'Ox = AC, Oy starts in A-point and perpendicular Ox
        yPMI = sqr(a^2 -((a^2+c^2-b^2)/2/c)^2)/3
    End Property
       
    '======================================
    '4.Funs + Subs:
    '======================================
	
    Public Function calcPerimetr(a, b, c)
        calcPerimetr = a + b + c
    End Function
   
    Public Function calcSquare(a, b, c)
        calcSquare = Sqr((a+b+c)/2*((a+b+c)/2-a)*((a+b+c)/2-b)*((a+b+c)/2-c))
    End Function
'=======================================================================================   
End Class
Dim userTriangle

'Say Hello
MsgBox "I can calculate perimetr, square and coordinates of point of median intersection (PMI) for your triangle." & vbCr & vbCr & "So in next steps you can enter lenghts of sides or ""Exit"" to exit program", vbOkOnly, "Hello!"
	
Do
	Set userTriangle = New Triangle
	
	'Set a,b,c
	userTriangle.setSideLenght = 0
	a = lcase(trim(userTriangle.getSideLenght(0)))
	If a = "exit" Then
		contine = MsgBox ("Ok, do you want try again?", vbYesNo)
	Else
		userTriangle.setSideLenght = 1
		b = lcase(trim(userTriangle.getSideLenght(1)))
		If b = "exit" Then
			contine = MsgBox ("Ok, do you want try again?", vbYesNo)
		Else
			userTriangle.setSideLenght = 2
			c = lcase(trim(userTriangle.getSideLenght(2)))
			If c = "exit" Then
				contine = MsgBox ("Ok, do you want try again?", vbYesNo)	
			Else
				a = cint(a) : b = cint(b) : c = cint(c)
				'Decide, calc or not to calc
				If a + b > c and b + c > a and c + a > b Then
	
				'calc parametres
				contine = MsgBox ( "Triangle with a = " & a & ", b = " & b & " and c = " & c & " :" & vbCr & vbCr &_
								   "Perimetr = " & userTriangle.perimetr & vbCR &_
								   "Square    = " & FormatNumber(userTriangle.square, 2) & vbCR &_
								   "Coordinates of point of median intersection: ( " & FormatNumber(userTriangle.xPMI, 2) & "; " & FormatNumber(userTriangle.yPMI, 2) & " )" & vbCr & vbCr & "Calc one more time?", vbYesNo + 64, "Happy End!")
		  		  Set userTriangle = Nothing
				Else
					contine = MsgBox("Actually, sum of each two sides must be more than third side. Do you want try again?", vbYesNo, "no no no -_-")
				End if
			End If
		End If
	End If
Loop Until contine = vbNo
	