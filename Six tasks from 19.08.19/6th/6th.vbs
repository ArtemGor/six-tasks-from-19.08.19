'===================================================================================
'6.+ Описать класс «домашняя библиотека»,
'  + Описать класс «книга»,
'  + Описать класс «xml loader»,
'  + Предусмотреть возможность работы с произвольным числом книг,
'  + Предусмотреть возможность поиска книги по автору и названию,
'  + Предусмотреть добавление книг в библиотеку,
'  + Предусмотреть удаление книг из нее,
'  + Предусмотреть возможность просмотра всей библиотеки
'  + Заполнить изначальную библиотеку следующим списком, взятых из текстового файла(данные хранить в формате XML):'
'===================================================================================

path = InputBox("Доброго дня! Введите, пожалуйста, путь к xml-файлу библиотеки(HomeLibrary.xml)")

Class LibraryList
    'Class WorkList describes ways to Add, Find, Delete, See all books and Save changes
    '======================================
    '1.Свойства и переменные
    '======================================
    Private Xmlworker
    Public List
    Public foundI
    
    '======================================
    '2.Конструктор/деструктор
    '======================================
    Private Sub Class_Initialize()
        Set xmlWorker = New xmlDownloader
        Call xmlWorker.GetListFromXML(List, path)
    End Sub

    Private Sub Class_Terminate()
        Set List = Nothing
    End Sub    

    '======================================
    '3.Getters+Setters
    '======================================

    '=========================================
    '4.Funs + Subs:
    'Funs: LibReview, DeleteBook and DeleteBook, SortBooks
    'Subs: AddBook, SaveChanges,
    '=========================================
    
    Function ShowError(XMLDOMParseError)
        mess = _
        "parseError.errorCode: " & XMLDOMParseError.errorCode & vbCrLf & _
        "parseError.filepos: " & XMLDOMParseError.filepos & vbCrLf & _
        "parseError.line: " & XMLDOMParseError.line & vbCrLf & _
        "parseError.linepos: " & XMLDOMParseError.linepos & vbCrLf & _
        "parseError.reason: " & XMLDOMParseError.reason & vbCrLf & _
        "parseError.srcText: " & XMLDOMParseError.srcText & vbCrLf & _
        "parseError.url: " & XMLDOMParseError.url & vbCrLf
        Msgbox mess
    End Function
    
    Function FindBook(searchRequest)
        foundFlag = 0
        Result = "Books in library, matched with " & searchRequest & vbCr
        Set RegExp = CreateObject("VBScript.RegExp")
        RegExp.Global = True
        RegExp.IgnoreCase = True
        RegExp.Pattern = searchRequest
        Set foundI = CreateObject("Scripting.Dictionary")
        For i = 0 to List.Count - 1
            a = List.items
            If RegExp.test(a(i).getNameBook) or RegExp.test(a(i).getAuthorBook) Then
                Result = Result & a(i).getNameBook & " " & a(i).getAuthorBook &  vbCr
                foundI.Add foundI.Count + 1, i
                foundFlag = 1
            End If
        Next
        
        If foundFlag = 0 Then
            FindBook = "There is no such book in library"  
        ElseIf foundFlag = 1 Then
            FindBook = Result
        End If
    
    End Function

    Sub AddBook(targetName, targetAuthor)
        Dim newBook : Set newBook = New Book
        newBook.setNameBook(targetName)
        newBook.setAuthorBook(targetAuthor)
        List.Add List.Count+1, newBook
        MsgBox "Book '" & targetName & "' of the author " & targetAuthor & " added to library."
    End Sub
    
    Function DeleteBook(searchRequest)
        If FindBook(searchRequest) = "There is no such book in library" Then
            DeleteBook = "There is no such book in library"
        ElseIf FindBook(searchRequest) <> "There is no such book in library" Then
            a = List.items
            If foundI.Count = 1 Then
                List.Remove(foundI.Item(foundI.Count))                
                DeleteBook = "Deleting done"
            ElseIf foundI.Count > 1 Then
                Msgbox foundI.Count & " : " & foundI.Item(0) &" "& foundI.Item(1) &" "& foundI.Item(2)
                Do
                    textToChoose = ""
                    For j = 1 To foundI.Count - 1
                        textToChoose = textToChoose & j & " " & a(foundI.Item(j)).getNameBook & " " & a(foundI.Item(j)).getAuthorBook &  vbCr
                    Next
                    textToChoose = textToChoose & foundI.Count & " For exit"
                    choosenToDelete = InputBox(" Enter book's number you want to delete" & vbCr & textToChoose)
                Loop Until isNumeric(choosenToDelete) 
                If CInt(choosenToDelete) = foundI.Count+1 Then
                        DeleteBook = "Deletening canceled"
                    Else
                    'Найти номер книги, который надо удалить
                    ' x -номер книги в отфильтрованном списке
                    x = choosenToDelete
                    b = foundI.Items
                    y = b(choosenToDelete)
                        List.Remove(b(choosenToDelete-1))    
                        DeleteBook = "Deleting done"                        
                End If
            End If
        End If
    End Function
    
    Function LibReview()
    
        a = List.Keys
        b = List.Items
        LibReview = "Library Review:" & vbCr & "Number |       Author     |           Name" &  vbCr
        For i = 0 to List.Count - 1
            LibReview = LibReview & CInt(a(i)) &  " | " & b(i).getAuthorBook & " | " & b(i).getNameBook &  " | " & vbCr
        Next
    End Function
    
    Sub SaveChanges
        Set xmlWorker = New xmlDownloader
        Call xmlWorker.SaveListToXML(List, TestArgs("libSource"))    
    End Sub
    
End class


Class Book
    'Class Book describes ways to get name and author of book and to delete book  
    
    '======================================
    '1.Свойства и переменные
    '======================================
    
    Private nameBook
    Private authorBook
    
    '======================================
    '2.Конструктор/деструктор
    '======================================

    '======================================
    '3.Getters+Setters
    '======================================
    
    '======================================
    '4.Funs + Subs:
    '======================================
    Function setNameBook(x)
        nameBook = x
    End Function
    Function getNameBook
        getNameBook = nameBook
    End Function
    Function setAuthorBook(x)
        authorBook = x
    End Function    
    Function getAuthorBook
        getAuthorBook = authorBook
    End Function
End Class             

Class xmlDownloader
    'Class xmlDownloader describes ways how to Get objects "Book" list and how to save changes back in xml data file
    '======================================
    '1.Свойства и переменные
    '======================================
    Public List
    
    '======================================
    '2.Конструктор/деструктор
    '======================================
    
    Sub Class_Initialize()
    End Sub
    
    Sub Class_Terminate()
        Set xmlParser = Nothing
    End Sub
    
    '======================================
    '3.Getters+Setters
    '======================================

    '=========================================
    '4.Funs + Subs:
    '=========================================
    
    Public Sub GetListFromXML(List, xpath)
        '1.Download xml-file and transform it into list of books
        Set xmlParser = CreateObject("Msxml2.DOMDocument")
        xmlParser.async = False
        xmlParser.load xpath
        If xmlParser.parseError.errorCode Then
            ShowError xmlParser.parseError
        End If
        Set List = CreateObject("Scripting.Dictionary")
        Set currNode = xmlParser.documentElement
        Set currNode = currNode.firstChild
        Dim myBook        
        While Not currNode Is Nothing
            Set myBook = New Book
            myBook.setNameBook(currNode.childNodes(1).text)
            myBook.setAuthorBook(currNode.childNodes(0).text)
            List.Add i, myBook
            Set currNode = currNode.nextSibling        
            i = i + 1
        Wend    
    End Sub
    
    Public Sub SaveListToXML(List, xpath)
        '1.Create string of elements
        stringForXML = "<?xml version=""1.0"" encoding=""windows-1251""?>" & vbCr & _
                       "<HomeLibrary>" & vbCr
        a = List.Items
        For i = 0 To List.Count - 1
        stringForXML = stringForXML & _
        chr(9) & "<book>" & vbCr &_
        chr(9) & chr(9) & "<Name>" & a(i).getNameBook & "</Name>" & vbCr &_
        chr(9) & chr(9) & "<Author>" & a(i).getAuthorBook & "</Author>" & vbCr &_
        chr(9) & "</book>" & vbCr
        Next
        stringForXML = stringForXML & "</HomeLibrary>"
        
        '3.Save as xml
        Dim fso: Set fso = CreateObject("Scripting.FileSystemObject")
        Dim f1: Set f1 = fso.OpenTextFile(xpath, 2)
        f1.Write stringForXML
        f1.Close
        MsgBox "Changes saved"
    End Sub
End Class

'Body
'1.Create LybraryList object

Set MyLittleLibrary = New LibraryList

'2.Run MainMenu and wait user answer

Do
    Do
        'Main menu
        Set objBook = New Book
        mainMenuNumber = InputBox ("Welcome to HomeLibrary" & vbCr &_
        "Input number, what you want to do:" & vbCr & vbCr &_
        "1.Find book." & vbCr &_    
        "2.Add book." & vbCr &_
        "3.Delete book." & vbCr &_    
        "4.See library." & vbCr &_
        "5.Save changes." & vbCr &_
        "6.Exit library")

    'Protect from wrong input
    If NOT isNumeric(mainMenuNumber) Then
        MsgBox "Please, answer in numeric format"
    End If
    Loop Until isNumeric(mainMenuNumber)

    'Ask addition parametres, if needed
    If mainMenuNumber = 1 Then
        searchRequest = InputBox("Enter search request:")
    End If

    If mainMenuNumber = 2 Then
        targetAuthor = InputBox("Enter book's author:")
        targetName = InputBox("Enter book's name:")
    End If
    
    If mainMenuNumber = 3 Then
        searchRequest = InputBox("Enter request for search deleting book:" & vbCr & "If you enter author's data all books of this author will be deleted")
    End if    
    
    
    'Decide what to do
    if mainMenuNumber = 1 then
            MsgBox MyLittleLibrary.FindBook(searchRequest)
        Elseif mainMenuNumber = 2 then
            Call MyLittleLibrary.AddBook(targetName, targetAuthor)
        Elseif mainMenuNumber = 3 then
            MsgBox MyLittleLibrary.DeleteBook(searchRequest)
        Elseif mainMenuNumber = 4 then
            MsgBox MyLittleLibrary.LibReview
        Elseif mainMenuNumber = 5 then
            Call MyLittleLibrary.SaveChanges
        Elseif mainMenuNumber <> 6 then
            MsgBox "You shall not pass!"
        End if
Loop Until mainMenuNumber = 6
