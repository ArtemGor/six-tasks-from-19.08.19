
'==============================================================================================
'2.	Дана любая строка на вход. Заменить все ссылки и email на ***** (количество звездочек равно длине заменяемого фрагмента). Примеры ссылок: www.site.com, http://site.com и т.п. Решить двумя способами: с использованием регулярных выражений и без. Сравнить скорости работы.
'==============================================================================================
'Test stringe:

'2.Без использования регулярных выражений
Option Explicit
dim inputString, outputString, my_arr, index, lengthIndex, separator, outStr, j, b(), k
'дана строка 
inputString = "sfouijhn EMAIL> aef@sea.co weifhu SITE> https://sa5.cm dsfghsdhgetr WWWx EMAIL> 43@sg.ru erg 456 SITE> www.shr.com shwert  EMAIL> f522@47hi.ru a432 SITE> dfghrsth.ua"                'InputBox("Please, input the string:")
'массив = разбиение строки на элементы
separator = " "

my_arr = split(inputString, separator)
'делаем список из элементов массива
k = -1
For each index in my_arr
	if InStr(index, "@") > 0 or InStr(index, "http://") > 0 and InStr(index, ".") or InStr(index, "https://") > 0 and InStr(index, ".") or InStr(index, "www.") and InStr(index, ".") > 0 then
		lengthIndex = Len(index)
		k = k + 1
		For j = 1 To lengthIndex - 1 Step 1
			Redim Preserve b(k)
			b(k) = b(k) & "*"
		Next
		MsgBox b(k)
		index = b(k)
	End if
	outStr = outStr & " " & index
Next

MsgBox outStr 
