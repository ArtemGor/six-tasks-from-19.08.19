
'==============================================================================================
'2.	Дана любая строка на вход. Заменить все ссылки и email на ***** (количество звездочек равно длине заменяемого фрагмента). Примеры ссылок: www.site.com, http://site.com и т.п. Решить двумя способами: с использованием регулярных выражений и без. Сравнить скорости работы.
'==============================================================================================
'With RegExp
'Test stringe:

Option Explicit
Dim inputString, outputString, RegExp, i, j, Match, Matches, a(), b()

inputString = "sfouijhn EMAIL> aef@sea.co weifhu SITE> https://sa5.cm dsfghsdhgetr WWWx EMAIL> 43@sg.ru erg 456 SITE> www.shr.com shwert  EMAIL> f522@47hi.ru a432 SITE> dfghrsth.ua"    'InputBox("Please, input the string:")

'1.С использованием регулярных выражений

Set RegExp = CreateObject("VBScript.RegExp")
RegExp.Global = True
RegExp.IgnoreCase = True


'Задать шаблон для поиска
RegExp.Pattern = "(([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6}))|(((https?:\/\/)|(www.))?([\da-z\.-]+)\.([a-z\.]{2,6}))"

'Находим соответствия и заносим их в коллекцию Matches
Set Matches = RegExp.Execute(inputString)
For i = 0 To Matches.Count - 1 step 1
	'Каждому значению из Matches создаём равный элемент массива a(i) и равный по длине элемент массива b(i) из *
	Set Match = Matches.Item(i)
	Redim Preserve a(i)
	a(i) = match
	For j = 0 to match.length step 1
		Redim Preserve b(i)
		b(i) = (b(i) & "*")
	Next
Next

outputString = inputString

'заменяем элементы из первоначальной строки, равные a(i) на b(i)
For i = 0 To Matches.Count - 1 step 1
	outputString = Replace(outputString, a(i), b(i), 1, 1)	
Next

'Вывести результат
MsgBox outputString