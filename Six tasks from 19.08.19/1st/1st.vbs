'============================================================================
'1.	Дан файл с логинами и паролями. Найдите топ10 самых популярных паролей и поместить их в текстовый файл на рабочем столе, в порядке их рейтинга (от самого популярного к менее). 
'============================================================================
Option Explicit
Const ForReading = 1, ForWriting = 2, ForAppending = 8
Dim ReadAllTextFile, RegExp, Res, A, i, j, k,  List, mas(), max(), stoitLiPropystit, j_iskl(), TOP, input
Dim fso: Set fso = CreateObject("Scripting.FileSystemObject")
input = InputBox("Введите путь к файлу с логинами и паролями")
Dim f1: Set f1 = fso.OpenTextFile(input, 1) 
input = InputBox("Введите путь к файлу, в который надо будет сохранить результат")
Dim f2: Set f2 = fso.CreateTextFile(input, 2)
ReadAllTextFile = f1.ReadAll

Set RegExp = CreateObject("VBScript.RegExp")
RegExp.Global = True

'Заменить все ";" на ""
RegExp.Pattern = ".*;"
Res = RegExp.Replace(ReadAllTextFile, "")

'сформировать из них 1 массив c колонками (пароль)(частота повторений)
A = Split(Res, chr(13), -1, 1)

For i = 0 To uBound(A)
	ReDim Preserve mas(1,uBound(A))
	mas(0,i) = A(i)                 'это пароли
	mas(1,i) = 1                    'это количества повторений
Next

'находим повторяющиеся пароли и заносим их количество
'бежим по паролям и 

For i = 0 to ubound(A)
	For j = i + 1 To ubound(A)
		'каждый сравнить со всеми последующими, если находится равный - увеличить значение частоты повторений искомого и установить найденному количество повторений на 0, если не находится - ничего не делать.
		If mas(1,j) <> 0 Then
			If mas(0,i) = mas(0,j) Then
				mas(1,i) = mas(1,i) + 1
				mas(1,j) = 0
			End If
		End If
	Next
Next

'из сформированного массива составить массив из элементов, выставленных по рейтингу
 TOP = 9
 i = 0
 ReDim Preserve j_iskl(0)
 For i = 0 to TOP               'для полного списка - uBound(A)
 	ReDim Preserve max(1,i)
 	max(1,i) = 0
 	For j = 1 to uBound(A)     'для полного списка - uBound(A)
	 	stoitLiPropystit = 0
	 	For k = 0 To uBound(j_iskl)
 			If j = j_iskl(k) Then
	 			stoitLiPropystit =  stoitLiPropystit + 1
 			End If
 		Next
 		If stoitLiPropystit = 0 Then
	 		If mas(1,j) > 0 Then
 				If max(1,i) < mas(1,j) or max(1,i) = mas(1,j) and max(0,i) <> mas(0,j) Then
	 				max(1,i) = mas(1,j) : max(0,i) = mas(0,j) : ReDim Preserve j_iskl(i) : j_iskl(i) = j
 				End If 				
 			End If
 		End If
 	Next
Next

List = ""
For i = 0 to TOP
	If max(1,i) <> 0 Then
		List = List & max(0,i) & " " & max(1,i) & vbCr
	End If
Next

'сохранить его в файлe
f2.Write List
f2.Close


