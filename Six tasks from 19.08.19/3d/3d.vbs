'==========================================================================================
'3.	Дан массив. Перемешать его элементы случайным образом так, чтобы каждый элемент оказался на новом месте.
'==========================================================================================
Option Explicit
Randomize
Dim A(), B(), columnsA, stringsA, List, i, j, ii, jj, answer

Msgbox "Программа позволяет перемешать элементы массива случайным образом так, чтобы каждый элемент оказался на новом месте", vbOkOnly, "Доброго дня!"

Do
	'Ввод значений элементов массива
	Do
		stringsA = CInt(InputBox("Введите количество строк:")) - 1
		columnsA = CInt(InputBox("Введите количество колонок:")) - 1
	Loop Until stringsA >= 0 and columnsA >= 0
	
	ReDim Preserve A(columnsA, stringsA)
	For i = 0 To stringsA
		For j = 0 To columnsA
			A(j,i) = InputBox("Введите значение элемента " & i + 1 &"-ой строки и "& j + 1 & "-ой колонки:")
		Next
	Next
	
	List = ""
	For i = 0 to stringsA
		For j = 0 To columnsA
			List = (List & A(j,i) & " ")
		Next
		List = List & vbCr
	Next
	
	MsgBox "Дан массив:" & vbcr & List
	
	'Перемешиваем элементы
	ReDim Preserve B(columnsA, stringsA)
	
	For i = 0 To stringsA Step 1
		For j = 0 To columnsA
			Do
			ii = Int(Rnd()*(stringsA+1))
			jj = Int(Rnd()*(columnsA+1))		
			Loop Until B(jj,ii) = ""
			B(jj,ii) = A(j,i)
		Next
	Next
	
	List = ""
	For i = 0 to stringsA
		For j = 0 To columnsA
			List = (List & B(j,i) & " ")
		Next
		List = List & vbCr
	Next
	
	'Выводим результат
	answer = MsgBox ("Массив после перемешивания:" & vbcr & List & vbcr & "Выполнить задание ещё раз?", vbYesNo)
	
Loop Until answer = vbNo
	
	
	
	




