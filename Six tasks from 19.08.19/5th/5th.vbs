'====================================================================================
'5.	Класс Покупатель: Фамилия, Имя, Отчество, Адрес, Номер кредитной карточки, Номер банковского счета;
' * Рассказать подробно, что такое Конструктор и деструктор;
' * Методы: установка значений атрибутов, получение значений атрибутов, вывод информации.
' + Создать массив объектов данного класса.
' + Вывести список покупателей в алфавитном порядке
' + Вывести список покупателей, у которых номер кредитной карточки находится в заданном диапазоне.
' + (Заполнять данные из текстового файла – данные хранить в формате XML)
'  
'===================================================================================
Option Explicit
Class MyClass
'===================================================================================
'1 dims

	Dim x, i, j, k, xmlParser, firstnameNodes, surnameNodes, fathersnameNodes, addressNodes, ccnNodes, banNodes, Firstname, Surname, FathersName, Address, CreditCardNumber, BankAccountNumber, A(), iskl(), List, stoitLiPropystit, downLimit, upLimit

'===================================================================================
'2.Construct/destruct
	Private Sub Class_Initialize()
		Set xmlParser = Nothing
		x = "N:\Common files\Distrib\Backups\ArtBackup\Артём\4. Шесть задач от 19.08.19\Customers.xml"
		Set xmlParser = CreateObject("Msxml2.DOMDocument")
		xmlParser.async = False
		xmlParser.Load x

		'2.Checks
		'first loadcheck
		if xmlParser.load(x) = false Then MsgBox "download false" End if
		'second loadcheck
		If xmlParser.parseError.errorCode Then ShowError oXMLFile.parseError End If
		
		'3.Create massive to operate
		Set surnameNodes = xmlParser.selectNodes("//Customer/Surname")
		Set firstnameNodes = xmlParser.selectNodes("//Customer/FirstName")
		Set fathersnameNodes = xmlParser.selectNodes("//Customer/FathersName")
		Set addressNodes = xmlParser.selectNodes("//Customer/Address")
		Set ccnNodes = xmlParser.selectNodes("//Customer/CreditCardNumber")
		Set banNodes = xmlParser.selectNodes("//Customer/BankAccountNumber")
		ReDim Preserve A(firstnameNodes.Length+1, firstnameNodes.Length+1)

		List = ""
		For i = 1 to firstnameNodes.Length+1
			Surname = surnameNodes.item(i-1).text
			FirstName = firstnameNodes.item(i-1).text
			FathersName = fathersnameNodes.item(i-1).text
			Address = addressNodes.item(i-1).text
			CreditCardNumber = ccnNodes.item(i-1).text
			BankAccountNumber = banNodes.item(i-1).text
			A(i,0) = Surname
			A(i,1) = FirstName
			A(i,2) = FathersName
			A(i,3) = Address
			A(i,4) = CreditCardNumber
			A(i,5) = BankAccountNumber
		Next
	End Sub
	
	Sub Class_Terminate()
		'Clear memory!
		Set xmlParser = Nothing
	End Sub
	
'===================================================================================
'3.fun/proc
	Function ShowError(XMLDOMParseError)
	mess = _
	"parseError.errorCode: " & XMLDOMParseError.errorCode & vbCrLf & _
	"parseError.filepos: " & XMLDOMParseError.filepos & vbCrLf & _
	"parseError.line: " & XMLDOMParseError.line & vbCrLf & _
	"parseError.linepos: " & XMLDOMParseError.linepos & vbCrLf & _
	"parseError.reason: " & XMLDOMParseError.reason & vbCrLf & _
	"parseError.srcText: " & XMLDOMParseError.srcText & vbCrLf & _
	"parseError.url: " & XMLDOMParseError.url & vbCrLf
'	WScript.Echo mess
End Function

'===================================================================================
'4.properies
	Property Get ShowMassive
		List = "Massive of class objects: " & vbCr
		For i = 1 to firstnameNodes.Length
			List = List & A(i,0) & " " & A(i,1) & " " & A(i,2) & " " & A(i,3) & " " & A(i,4) & " " & A(i,5) & vbCr
		Next
		ShowMassive = List
	End Property
	
	Property Get CustomersInFirstNameAbcSort
	List = "Customers List In FirstName-Abc-Sort: " & vbCr
		ReDim Preserve iskl(0)
 		For i = 0 to firstnameNodes.Length
 			ReDim Preserve max(i)
 			max(i) = 0
 			For j = 1 To firstnameNodes.Length
			stoitLiPropystit = 0
			ReDim Preserve iskl(i)
			For k = 0 To uBound(iskl)
 				If j = iskl(k) Then
			 		stoitLiPropystit = stoitLiPropystit + 1
 				End If
 			Next
 			If stoitLiPropystit = 0 Then
 				If  A(j,0) > max(i) Then
					max(i) = A(j,0)
					iskl(i) = j
 				End If 				
		 	End If
		 	Next
	 		List = List & A(iskl(i),0) & " " & A(iskl(i),1) & " " & A(iskl(i),2) & " " & A(iskl(i),3) & " " & A(iskl(i),4) & " " & A(iskl(i),5) & vbCr
	 	Next
	 	CustomersInFirstNameAbcSort = List
	End Property
	
	Property Get CustomersListAfterCCNFilter
		downLimit = CDbl(InputBox("Input down limit for credit card number filter:"))
		upLimit = CDbl(InputBox("Input up limit for credit card number filter:"))
		List = "Customers list after credit card number filter" & vbCr & "(> "& downLimit & " and <" & upLimit & ")" & vbCr
		For i = 0 to firstnameNodes.Length
			If A(i,4) > downlimit and A(i,4) < uplimit Then
				List = List & A(i,0) & " " & A(i,1) & " " & A(i,2) & " " & A(i,3) & " " & A(i,4) & " " & A(i,5) & vbCr
			End If
		Next
		CustomersListAfterCCNFilter = List
	End Property
End Class

Dim myObj
Set myObj = New MyClass  'construct
MsgBox myObj.ShowMassive
MsgBox myObj.CustomersInFirstNameAbcSort
MsgBox myObj.CustomersListAfterCCNFilter
myObj = 0 'destruct Loop Until MsgBox("Посчитать ещё раз?", vbYesNo) = vbNo